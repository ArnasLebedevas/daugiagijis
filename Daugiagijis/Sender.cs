﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Daugiagijis
{
    class Sender
    {
        //private string fileName;
        List<string> fileName;

        public Sender(List<string> fileName)
        {
            this.fileName = fileName;

        }

        public void Send(/*int fileCount*/)
        {
            fileName.ForEach(Console.WriteLine);

            IPAddress receiverIP = IPAddress.Parse("127.0.0.1");
            IPEndPoint endPoint = new IPEndPoint(receiverIP, 2021);
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(endPoint);


            foreach (string fileName in fileName)
            {
              
                byte[] fileNameBytes = Encoding.ASCII.GetBytes(Path.GetFileName(fileName));
                byte[] fileNameLength = BitConverter.GetBytes(Path.GetFileName(fileName).Length);
                byte[] fullBuffer = new byte[4 + fileNameBytes.Length];

                fileNameLength.CopyTo(fullBuffer, 0);
                fileNameBytes.CopyTo(fullBuffer, 4);


                socket.SendFile(fileName, fullBuffer, null, TransmitFileOptions.UseDefaultWorkerThread);
                
                //for confirming
  Console.WriteLine("SENT");
                

            }

            socket.Close();
        }
    }
}
