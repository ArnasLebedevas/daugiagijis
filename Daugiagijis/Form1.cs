﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Daugiagijis
{
    public partial class Form1 : Form
    {
        //string fileName = "";
        Receiver receiver = null;
        bool isCancelRequested = false;
         int fileCount = 0;
        //list of files
        List<string> fileName = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void UploadButton_Click(object sender, EventArgs e)
        {
     
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {


                foreach (string s in openFileDialog.FileNames)
                {
                    fileCount++;

                    if (fileCount > 5) { break; }
                    fileName.Add(s);
                    filesLabel.Text += Environment.NewLine + s;

                }
            }

            //pasirinkti failai
          //  fileName.ForEach(Console.WriteLine);
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            //foreach (string file in fileName)
            //{
                Sender sndr = new Sender(fileName);
                sndr.Send(/*fileCount*/);
            //}
        }

        private void StartReceiverButton_Click(object sender, EventArgs e)
        {
            isCancelRequested = false;
            Thread receiverThread = new Thread(() =>
            {
                receiver = new Receiver(Environment.ExpandEnvironmentVariables(@"C:\Users\inga3\OneDrive\Stalinis kompiuteris"));
                receiver.Start();
            });

            receiverThread.Start();

            Thread uiThread = new Thread(() =>
            {
                while (true)
                {
                    ReceiverLabel.Invoke((MethodInvoker)(() => ReceiverLabel.Text = Receiver.Message));
                    Thread.Sleep(100);
                    if (isCancelRequested)
                        break;
                }
            });

            uiThread.Start();
        }

        private void StopReceiverButton_Click(object sender, EventArgs e)
        {
            if (receiver != null)
                receiver.Stop();
            isCancelRequested = true;
        }
    }
}
