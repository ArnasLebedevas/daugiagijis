﻿
namespace Daugiagijis
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UploadButton = new System.Windows.Forms.Button();
            this.SendButton = new System.Windows.Forms.Button();
            this.FileNameLabel = new System.Windows.Forms.Label();
            this.StartReceiverButton = new System.Windows.Forms.Button();
            this.StopReceiverButton = new System.Windows.Forms.Button();
            this.ReceiverLabel = new System.Windows.Forms.Label();
            this.filesLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UploadButton
            // 
            this.UploadButton.Location = new System.Drawing.Point(47, 53);
            this.UploadButton.Name = "UploadButton";
            this.UploadButton.Size = new System.Drawing.Size(75, 23);
            this.UploadButton.TabIndex = 0;
            this.UploadButton.Text = "Upload";
            this.UploadButton.UseVisualStyleBackColor = true;
            this.UploadButton.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // SendButton
            // 
            this.SendButton.Location = new System.Drawing.Point(47, 178);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(75, 23);
            this.SendButton.TabIndex = 1;
            this.SendButton.Text = "Send";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // FileNameLabel
            // 
            this.FileNameLabel.AutoSize = true;
            this.FileNameLabel.Location = new System.Drawing.Point(81, 114);
            this.FileNameLabel.Name = "FileNameLabel";
            this.FileNameLabel.Size = new System.Drawing.Size(0, 13);
            this.FileNameLabel.TabIndex = 2;
            // 
            // StartReceiverButton
            // 
            this.StartReceiverButton.Location = new System.Drawing.Point(424, 53);
            this.StartReceiverButton.Name = "StartReceiverButton";
            this.StartReceiverButton.Size = new System.Drawing.Size(85, 23);
            this.StartReceiverButton.TabIndex = 3;
            this.StartReceiverButton.Text = "Start Receiver";
            this.StartReceiverButton.UseVisualStyleBackColor = true;
            this.StartReceiverButton.Click += new System.EventHandler(this.StartReceiverButton_Click);
            // 
            // StopReceiverButton
            // 
            this.StopReceiverButton.Location = new System.Drawing.Point(424, 178);
            this.StopReceiverButton.Name = "StopReceiverButton";
            this.StopReceiverButton.Size = new System.Drawing.Size(85, 23);
            this.StopReceiverButton.TabIndex = 4;
            this.StopReceiverButton.Text = "Stop Receiver";
            this.StopReceiverButton.UseVisualStyleBackColor = true;
            this.StopReceiverButton.Click += new System.EventHandler(this.StopReceiverButton_Click);
            // 
            // ReceiverLabel
            // 
            this.ReceiverLabel.AutoSize = true;
            this.ReceiverLabel.Location = new System.Drawing.Point(468, 114);
            this.ReceiverLabel.Name = "ReceiverLabel";
            this.ReceiverLabel.Size = new System.Drawing.Size(0, 13);
            this.ReceiverLabel.TabIndex = 5;
            // 
            // filesLabel
            // 
            this.filesLabel.AutoSize = true;
            this.filesLabel.Location = new System.Drawing.Point(47, 223);
            this.filesLabel.Name = "filesLabel";
            this.filesLabel.Size = new System.Drawing.Size(31, 13);
            this.filesLabel.TabIndex = 6;
            this.filesLabel.Text = "Files:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 365);
            this.Controls.Add(this.filesLabel);
            this.Controls.Add(this.ReceiverLabel);
            this.Controls.Add(this.StopReceiverButton);
            this.Controls.Add(this.StartReceiverButton);
            this.Controls.Add(this.FileNameLabel);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.UploadButton);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UploadButton;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.Button StartReceiverButton;
        private System.Windows.Forms.Button StopReceiverButton;
        private System.Windows.Forms.Label ReceiverLabel;
        private System.Windows.Forms.Label filesLabel;
    }
}

