﻿
using IronOcr;
using Patagames.Ocr;
using Patagames.Ocr.Enums;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Metadata;
using System.Text;
using MODI;
using Document = MODI.Document;

namespace Daugiagijis
{
    class Receiver
    {
        private static TcpListener listener = null;

        private string downloadsFolder;
        public static string Message = "Stopped";

        public Receiver(string downloadsFolder)
        {
            this.downloadsFolder = downloadsFolder;
            if (listener == null)
                listener = new TcpListener(IPAddress.Any, 2021);
        }

        [Obsolete]
        public void Start()
        {
            try
            {
               listener.Start();
                Message = "Started";

                while (true)
                {
                    using (var client = listener.AcceptTcpClient())
                    using (var stream = client.GetStream())
                    {
                        byte[] fileNameLengthBytes = new byte[4];
                        stream.Read(fileNameLengthBytes, 0, 4);
                        int fileNameLength = BitConverter.ToInt32(fileNameLengthBytes, 0);
                        byte[] fileNameBytes = new byte[fileNameLength];
                        stream.Read(fileNameBytes, 0, fileNameLength);
                        string fileName = Encoding.ASCII.GetString(fileNameBytes, 0, fileNameLength);

                        string file = $"{downloadsFolder}{fileName}";
                       // Console.WriteLine(file);


                        using (var output = File.Create(file))
                        {
                            Message = "Saving file...";
                            var buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                // progresui ideali vieta
                                output.Write(buffer, 0, bytesRead);
                            }
                        }
                        Message = "Saving file complete";




                        MODI.Document modiDocument = new Document();
                        modiDocument.Create(file);
                        modiDocument.OCR(MiLANGUAGES.miLANG_ENGLISH);
                        MODI.Image modiImage = (modiDocument.Images[0] as MODI.Image);
                        string extractedText = modiImage.Layout.Text;
                        //TEKSTAS IS FAILO:

                        Console.WriteLine(extractedText);

                        Console.WriteLine("---------------------------------------------------------------------------");



                    }
                }
            }
            catch (Exception exc)
            {
                Message = exc.Message;
            }
            finally
            {
                if (listener != null)
                    listener.Stop();
                Message = "Stopped";

            }
        }

        public void Stop()
        {
            listener.Stop();
            listener = null;
            Message = "Stopped";
        }
    }
}
